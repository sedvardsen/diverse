﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using AuthenticationMode = System.Web.Configuration.AuthenticationMode;

namespace MinSide.Web
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        public static AuthenticationMode AuthMode = AuthenticationMode.None;
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //TODO: Move to IoC-container and inject
            AuthenticationSection auth = (AuthenticationSection) WebConfigurationManager.GetSection("system.web/authentication");
            AuthMode = auth.Mode;
        }

        // Add claims for AD-User. This should probably be some sort of in-memory-lookup
        void Application_PostAuthenticateRequest()
        {
            if (Request.IsAuthenticated)
            {
                if (AuthMode == AuthenticationMode.Windows)
                {
                    AddClaimsForAdUser();
                }
            }
        }

        //TODO: Make proper class and interface, move to IoC-container and inject
        public static void AddClaimsForAdUser()
        {
            var id = ClaimsPrincipal.Current.Identities.First();
            id.AddClaim(new Claim("info-fra-MDM", "Dette er en ekte ansatt"));
        }

        //TODO: Make proper class and interface, move to IoC-container and inject
        public static void AddClaimsForAdfsUser(List<Claim> claims, AuthenticateResult result)
        {
            claims.Add(new Claim(ClaimTypes.AuthenticationMethod, "ADFS"));
            /*Fra MDM*/
            claims.Add(new Claim("info-fra-MDM", "Dette er en ekte bonde"));
            claims.Add(new Claim(ClaimTypes.Name, result.Identity.FindFirstValue(ClaimTypes.WindowsAccountName)));
            claims.Add(new Claim(ClaimTypes.NameIdentifier, result.Identity.FindFirstValue(ClaimTypes.WindowsAccountName)));
        }
    }
}
