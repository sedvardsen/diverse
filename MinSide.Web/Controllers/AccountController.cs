﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

using RedirectResult = System.Web.Mvc.RedirectResult;


namespace MinSide.Web.Controllers
{
    public class AccountController : Controller
    {


        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.Authorize]
        public ActionResult LoginAd()
        {
            return this.RedirectToAction("Index", "Home");
        }

        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.AllowAnonymous]
        public ActionResult LoginCallbackAdfs()
        {
            var ctx = Request.GetOwinContext();

            var result = ctx.Authentication.AuthenticateAsync(DefaultAuthenticationTypes.ApplicationCookie).Result;
            ctx.Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            if (result != null)
            {

                var claims = result.Identity.Claims.ToList();
                WebApiApplication.AddClaimsForAdfsUser(claims, result);

                var ci = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
                ctx.Authentication.SignIn(new AuthenticationProperties { IsPersistent = true }, ci);
            }
            return this.RedirectToAction("Index", "Home");
        }

       

        [System.Web.Mvc.HttpGet]
        public ActionResult SignOut()
        {
            var ctx = Request.GetOwinContext();
            ctx.Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

    }
}