﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Microsoft.Owin.Security;

using AuthenticationMode = System.Web.Configuration.AuthenticationMode;

namespace MinSide.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated == false)
            {
                var ctx = Request.GetOwinContext();

                // If windows authentication, let iis do the job
                // TODO: instead of checking on auth method, a check could (maybe) be done on web app location or some other logic
                if (WebApiApplication.AuthMode == AuthenticationMode.Windows)
                {
                   return RedirectToAction("LoginAd", "Account");
                }
                // Else go to adfs
                else 
                {
                    ctx.Authentication.Challenge(new AuthenticationProperties
                    {
                        RedirectUri = Url.Action("LoginCallbackAdfs", "Account")
                    }, "ADFS");
                }
            }

            ViewBag.Title = "Home Page";
            return View((User as ClaimsPrincipal).Claims);
        }
    }
}
