﻿using System.Net;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.WsFederation;
using Owin;

using Microsoft.Owin;

namespace MinSide.Web
{
    public partial class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {
            app.Properties["Microsoft.Owin.Security.Constants.DefaultSignInAsAuthenticationType"] = DefaultAuthenticationTypes.ApplicationCookie;
            var cookieAuthOptions = new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Active,
                SlidingExpiration = true,
            };
            

            app.UseCookieAuthentication(cookieAuthOptions);

            // TODO: Addresses and realms through configuration
            var adfs = new WsFederationAuthenticationOptions
            {
                MetadataAddress = "https://adfs01.bouvet.no/FederationMetadata/2007-06/FederationMetadata.xml",
                AuthenticationType = "ADFS",
                Caption = "Bouvet Adfs",
                BackchannelCertificateValidator = null,
                Wreply = "https://localhost:44300/Account/LoginCallbackAdfs",
                Wtrealm = "https://localhost:44300/Account/LoginCallbackAdfs",
                
            };
            //add to pipeline if not windows authentication
            var useAd = WebApiApplication.AuthMode == AuthenticationMode.Windows;
            if (useAd == false)
            {
                app.UseWsFederationAuthentication(adfs);
            }

            // Allow unsafe ssl-sites (TODO: Maybe turn off in production)
            ServicePointManager.ServerCertificateValidationCallback = delegate
            {
                return true;
            };

        }
    }
}